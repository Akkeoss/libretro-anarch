# Anarch

![](media/logo_big.png)

*extremely small, completely public domain, no-dependency, no-file,
portable suckless anarcho-pacifist from-scratch 90s-style Doom clone that runs
everywhere, made for the benefit of all living beings*

[website](https://drummyfish.gitlab.io/anarch) - [trailer](https://libre.video/videos/watch/c968774a-c12d-46d6-8851-0c578ffa8dcb) - [play in browser](https://drummyfish.gitlab.io/anarch/bin/web/anarch.html) - [itch.io](https://drummyfish.itch.io/anarch)

![](media/3screens.png)

**NOTE: for a more consoomerist experience there are now mods in the mod directory with which you may rice the game.** See `mods/consoomer_edition.sh`. You can now also make **tool assisted speedruns** with the demo mod. Now there is also a **mod that replaces the assets with Freedoom ones**: see [here](https://codeberg.org/drummyfish/drummy_utils/src/branch/master/anarch_fd) (separate repo as for different license). And we have a **cooperative multiplayer** mod too

![](media/riced.png)

![](media/screenshot_mp.png)

This game got some attention on 4chan: [1](https://archive.li/Yzcwt), [2](https://archive.li/xY4ia), [3](https://archive.li/tFWrL).

- [why this game is special](#why-this-game-is-special)
- [stats and comparisons (for version 1.0)](#stats-and-comparisons-for-version-1-0)
- [compiling](#compiling)
- [code guide](#code-guide)
- [usage rights](#usage-rights)

## Why this game is special

- **Completely public domain (CC0) free softare, free culture, libre game** for the benefit of all living beings in the Universe, **no conoditions on use whatsoever**. **All art is original** work and licensed CC0 (as well as code).
- **100% non-commercial**, free of any ads, spyware, microtransactions, corporate logos, planned obsolescence etc.
- **Extemely low HW demands** (much less than Doom, no GPU, no FPU, just kilobytes of RAM and storage).
- **Suckless, KISS, minimal, simple**, short code (~10000 LOC).
- **Extremely portable**. So far ported to and tested on:
  - GNU/Linux PC: SDL, csfml, terminal (experimental), Linux framebuffer (experimental)
  - OpenBSD SDL
  - FreeBSD SDL
  - Winshit XP SDL
  - Browser
  - Pokitto (220 x 116, 48 MHz ARM, 36 KB RAM, 256 KB flash)
  - Gamebuino Meta (80 x 64, 48 MHz ARM, 32 KB RAM, 256 KB flash)
  - Ringo/MAKERphone (160 x 128, 160 MHz ARM, 520 KB RAM, 4 MB flash)
  - ESPboy (128 x 128, 160 MHz)
  - Nibble (128 x 128, 160 MHz)
  - ncurses (not very playable)
  - unofficial [bare metal Raspberry Pi port](https://github.com/msx80/anarch-baremetalpi) by msx80
  - unofficial [DOS port](https://gitlab.com/wuuff/anarch-dos) by Wuuff
  - another unofficial [DOS port](https://codeberg.org/gera/anarch-dos) by gera
  - unofficial [Retrofw and Miyoo ports](https://github.com/szymor/anarch) by szymor
  - unofficially ran on/ported to [OpenPandora](https://git.sr.ht/~magic_sam/Anarch) by MagicSam ([forum thread](https://pyra-handheld.com/boards/threads/anarch.99869/#post-1714158))
  - unofficial [libretro port](https://codeberg.org/iyzsong/anarch-libretro) by iyzsong
  - rumor has it Anarch was also run in Emacs
- Has **completely NO external dependencies**, not even rendering or IO, that is left to each platform's frontend, but each frontend is very simple. Uses **no dynamic heap allocation** (no malloc).
- Can fit into **less than 256 kB (60 kB with procgen mod)** (including all content, textures etc.).
- Uses **no build system**, can typically be compiled with a single run of compiler (**single compilation unit**).
- **Works without any file IO**, i.e. can work without config files, save files, **all content and configs are part of the source code**.
- **Doesn't use any floating point**, everything is integer math (good for platforms without FPU).
- Written in **pure C99**, also a **subset of C++** (i.e. runs as C++ as well, good for systems that are hard C++ based).
- Made to **last for centuries** without maintenance.
- Goes beyond technical design and also **attempts to avoid possible cultural dependencies and barriers** (enemies are only robots, no violence on living beings).
- **Created with only free software** (GNU/Linux, GIMP, Audacity, gcc, Vim, ...).
- Uses a **custom-made 256 color palette** (but can run on platforms with fever colors, even just two).
- **Well documented and commented code**, written with tinkering and remixing in mind.
- Has the oldschool feel of games like **Doom** or **Wolf3D**.
- There is already a **number of mods** in the mod directory, of course all in the same spirit and under same conditions as the game. Don't forget to check them out :)

## Stats and comparisons (for version 1.0)

source code stats (80 column wrapping, a lot of empty lines):

| file                                  | LOC     | file size |
| --------------------------------------| ------- | --------- |
| game.h (main game logic)              | 4979    | 140 KB    |
| raycastlib.h (ray casting library)    | 2058    | 64 KB     |
| images.h, levels.h, sounds.h (assets) | 3868    | 340 KB    |
| settings.h, constants.h               | 1088    | 36 KB     |
| main_sdl.c (SDL frontend)             | 498     | 16 KB     |
| **total (cloc of all sources)**       | 14402   | 672 KB    |
| Doom source code                      | 38000   | 1.6 MB    |
| Wolf 3D source code                   | 25000   | 916 KB    |

compiled:

| binary               | size                                                                | target FPS         | resolution | RAM usage (total)  |
| -------------------- | ------------------------------------------------------------------- | ------------------ | ---------- | ------------------ |
| GNU/Linux, SDL       | 196 KB (gcc), 184 KB (clang), 184 KB (tcc), 57 KB (gcc procgen mod) | 60                 | 700 * 512  | ~40 MB             |
| GNU/Linux, terminal  | 184 KB (gcc), 176 KB (clang), 176 KB (tcc)                          | 30                 | 127 * 42   | ~5 MB              |
| Pokitto              | 180 KB                                                              | 35 (overclock), 22 | 110 * 88   | < 32 KB            |
| Gamebuino Meta       | 215 KB                                                              | 18                 | 80 * 64    | < 32 KB            |
| Ringo (MAKERphone)   | 1.3 MB                                                              | 35                 | 160 * 128  |                    |
| ESPboy               | 376 KB                                                              | 22                 | 128 * 128  |                    |
| Nibble               | 416 KB                                                              | 35                 | 128 * 128  |                    |
| browser              | 884 KB (whole output)                                               | 35                 | 512 * 320  | ~20 MB             |

system requirements:

| type               | Anarch                           | Wolf 3D                     | Doom                       |
| ------------------ | -------------------------------- | --------------------------- | -------------------------- |
| CPU                | ~40 MHz, 32 bit                  | Intel 286, ~10 MHz 16 bit   | Intel 386, ~40 MHz, 32 bit |
| RAM                | 32 KB                            | 528 KB                      | 4 MB                       |
| storage            | 200 KB (60 KB with a mod)        | 8 MB                        | 12 MB                      |
| display            | 60 x 32, 8 colors, double buffer | ?                           | ?                          |
| additional         | 7 buttons, frame buffer          | HDD, frame buffer           | HDD, frame buffer          |

features:

| type                          | Anarch                           | Wolf 3D           | Doom              |
| ----------------------------- | -------------------------------- | ----------------- | ----------------- |
| levels                        | 10                               | 60 (10 shareware) | 27 (9 shareware)  |
| variable floor/ceiling height | yes                              | no                | yes               |
| textured floor/ceiling        | no                               | no                | yes               |
| engine                        | advanced ray casting             |simple ray casting | BSP               |
| movement inertia              | no (yes with a mod)              | no                | yes               |
| head/weapon bobbing           | yes                              | no                | yes               |
| shearing (up/down look)       | yes                              | no                | no                |
| non-square based levels       | no                               | no                | yes               |
| jumping                       | yes                              | no                | no                |
| weapons                       | 6                                | 4                 | 8                 |
| enemy types                   | 7                                | 9                 | 10                |
| ammo types                    | 3                                | 1                 | 4                 |
| armor                         | no                               | no                | yes               |
| difficulty levels             | no                               | 4                 | 5                 |

## Compiling

Before compiling you may modify the game settings (change stuff in `settings.h`) and/or apply mods from the mod folder (e.g. with `git apply mods/full_zbuffer.diff` -- some diffs give warnings/errors but still work).

Then just compile your platform's frontend implementation, e.g. for GNU/Linux SDL2 compile `main_sdl.c` with C compiler and compile flags of your choice; of course you must have the packages/libraries required by the frontend (e.g. SDL2 for SDL frontend, `sudo apt-get install libsdl2-dev` etc.) installed and you have to link them during compilation (`-lSDL2` or use `sdl2-config` etc.).

To make this easier there is a simple compile script that should just work in most cases, it should be as simple as doing e.g.:

```
./make.sh sdl
```

There can be an extra parameter specifying the compiler, for example on OpenBSD I compiled this with  `./make.sh sdl cc`.

If you are on some exotic platform or have trouble compiling with the script, take a look at the script and try to fiddle with the flags etc., it is generally not hard to compile the game as you just need to compile a single file and there are multiple frontends to try. Still, if you're having real trouble, drop me an email :)

Also there are some precompiled binaries in the bin directory.

## Code guide

Most things should be obvious and are documented in the source code itself. This
is just an extra helper.

The repository structure is following:

```
assets/          asset sources (textures, sprites, maps, sounds, ...)
  *.py           scripts for converting assets to C structs/arrays
media/           media presenting the game (screenshots, logo, ...)
mods/            official mods
constants.h      game constants that aren't considered settings
game.h           main game logic
images.h         images (textures, sprites) from assets directory converted to C
levels.h         levels from assets directory converted to C
main_*.*         fronted implement. for various platforms, passed to compiler
palette.h        game 256 color palette
raycastlib.h     ray casting library
settings.h       game settings that users can change (FPS, resolution, ...)
smallinput.h     helper for some frontends and tests, not needed for the game
sounds.h         sounds from assets directory converted to C
texts.h          game texts
make.sh          compiling script constaining compiler settings
HTMLshell.html   HTML shell for emscripten (browser) version
index.html       game website
README.md        this readme
```

Main **game logic** is implemented in `game.h` file. All global identifiers of the game code start with the prefix `SFG_` (meaning *suckless FPS game*). Many identifiers also start with `RCL_` – these belong to the ray casting library.

The game core only implements the back end independent of any platforms and libraries. This back end has an API – a few functions that the front end has to implement, such as `SFG_setPixel` which should write a pixel to the platform's screen. Therefore if one wants to **port** the game to a new platform, it should be enough to implement these few simple functions and adjust game settings.

A deterministic **game loop** is used, meaning every main loop/simulation iteration has a constant time step, independently of actually achieved FPS. The FPS that is set determines both the target rendering FPS as well as the length of the simulation step: rendering at higher FPS than that of the simulation would bring no visual benefit as the draw function renders the game state as-is, without interpolating towards the next frame etc. Note that the game will behave slightly differently with different FPS values due to rounding errors, but should be okay as long as the FPS isn't extremely low (e.g. 2) or high (e.g. 1000). If the set  FPS can't be reached, the game will appear slowed down, so set the FPS to a value that your platform can handle.

The **rendering engine** -- raycastlib -- works on the principle of **ray casting** on a square grid and handles the rendering of the 3D environment (minus sprites). This library also handles player's **collisions** with the level. There is a copy of raycastlib in this repository but I maintain raycastlib as a separate project in a different repository, which you can see for more details about it. For us, the important functions interfacing with the engine are e.g. `SFG_floorHeightAt`, `SFG_ceilingHeightAt` (functions the engine uses to retirieve floor and ceiling height) and `SFG_pixelFunc` (function the engine uses to write pixels to the screen during rendering, which in turn uses each platform's specific `SFG_setPixel`). It is documented in its own source code.

Only **integer arithmetic** is used, no floating point is needed. Integers are effectively used as fixed point numbers, having `RCL_UNITS_PER_SQUARE` (1024) fractions in a unit. I.e. what we would write as 1.0 in floating point we write as 1024, 0.5 becomes 512 etc.

**Mods** of the vanilla version are recommended to be made as patches, so that they can easily be combined etc. If you want your mod to remain free, please don't forget a license/waiver, even for a small mod. You can never go wrong by including it.

**Sprite** (monster, items, ...) rendering is intentionally kept simple and doesn't always give completely correct result, but is good enough. Sprite scaling due to perspective should properly be done with respect to both horizontal and vertical FOV, but for simplicity the game scales them uniformly, which is mostly good enough. Visibility is also imperfect and achieved in two ways simultaneously. Firstly a 3D visibility ray is cast towards each active sprite from player's position to check if it is visible or not (ignoring the possibility of partial occlusion). Secondly a horizontal 1D z-buffer is used solely for sprites, to not overwrite closer sprites with further ones.

**Monster sprites** only have one facing direction: to the player. To keep things small, each monster has only a few frames of animations: usually 1 idle frame, 1 waling frame, 1 attacking frame. For dying animation a universal "dying" frame exists for all mosnters.

All ranged attacks in the game use **projectiles**, there is no hit scan.

The game uses a custom general purpose HSV-based 256 color **palette** which I again maintain as a separate project in a different repository as well (see it for more details). The advantage of the palette is the arrangement of colors that allows increasing/decreasing color value (brightness) by incrementing/decrementing the color index, which is used for dimming environment and sprites in the distance into shadow/fog without needing color mapping tables (which is what e.g. Doom did), saving memory and CPU cycles for memory access.

All **assets** are embedded directly in the source code and will be part of the compiled binary. This way we don't have to use file IO at all, and the game can run on platforms without a file system. Some assets use very simple compression to reduce the binary size. Provided python scripts can be used to convert assets from common formats to the game format.

All **images** in the game such as textures, sprites and backgrounds (with an exception of the font) are 32 x 32 pixels in 16 colors, i.e. 4 bits per pixel. The 16 color palette is specific to each image and is a subpalette of the main 256 color palette. The palette is stored before the image data, so each image takes 16 + (32 * 32) / 2 = 528 bytes. This makes images relatively small, working with them is easy and the code is faster than would be for arbitrary size images. One color (red) is used to indicate transparency.

The game uses a custom very simple 4x4 **font** consisting of only upper case letters and a few symbols, to reduce its size.

For **RNG** a very simple 8 bit congruent generator is used, with the period 256, yielding each possible 8 bit value exactly once in a period.

**AI** is very simple. Monsters update their state in fixed time ticks and move on a grid that has 4 x 4 points in one game square. Monsters don't have any direction, just 2D position (no height). Close range monsters move directly towards player and attack when close enough. Ranged monsters randomly choose to either shoot a projectile (depending on the aggressivity value of the monster's type) or move in random direction.

User/platform **settings** are also part of the source code, meaning that change of settings requires recompiling the game. To change settings, take a look at the default values in `settings.h` and override the ones you want by defining them before including `game.h` in your platform's front end source code.

To increase **performance**, you can adjust some settings, see settings.h and search for "performance". Many small performance tweaks exist. If you need a drastic improvement, you can set ray casting subsampling to 2 or 3, which will decrease the horizontal resolution of raycasting rendering by given factor, reducing the number of rays cast, while keeping the resolution of everything else (vertical, GUI, sprites, ...) the same. You can also divide the whole game resolution by setting the resolution scaledown, or even turn texturing completely off. You can gain or lose a huge amount of performance in your implementation of the `SFG_setPixel` function which is evaluated for every single pixel in every frame, so try to optimize here as much as possible. Also don't forget to use optimization flags of your compiler, they make the biggest difference. You can also try to disable music, set the horizontal resolution to power of 2 and similat things.

**Levels** are stored in levels.h as structs that are manually editable, but also use a little bit of compression principles to not take up too much space, as each level is 64 x 64 squares, with each square having a floor heigh, ceiling heigh, floor texture, ceiling texture plus special properties (door, elevator etc.). There is a python script that allows to create levels in image editors such as GIMP. A level consists of a tile dictionary, recordind up to 64 tile types, the map, being a 2D array of values that combine an index pointing to the tile dictionary plus the special properties (doors, elevator, ...), a list of up to 128 level elements (monsters, items, door locks, ...), and other special records (floor/ceiling color, wall textures used in the level, player start position, ...).

**Saving/loading** is an optional feature. If it is not present (frontend doesn't implement the API save/load functions), all levels are unlocked from the start and no state survives the game restart. If the feature is implemented, progress, settings and a saved position is preserved in permanent storage. What the permanent storage actually is depends on the front end implementation – it can be a file, EEPROM, a cookie etc., the game doesn't care. Only a few bytes are required to be saved. Saving of game position is primitive: position can only be saved at the start of a level, allowing to store only a few values such as health and ammo.

Performance and small size are achieved by multiple **optimization** techniques. Macros are used a lot to move computation from run time to compile time and also reduce the binary size. E.g. the game resolution is a constant and can't change during gameplay, allowing the compiler to precompute many expression with resolution values in them. Powers of 2 are used whenever possible. Approximations such as taxicab distances are used. Some "accelerating" structures are also used, e.g. a 2D bit array for item collisions. Don't forget to compile the game with -O3.

## Usage rights

**tl;dr: everything in this repository is CC0 + a waiver of all rights, completely public domain as much as humanly possible, do absolutely anything you want**

I, Miloslav Číž (drummyfish), have created everything in this repository, including but not limited to code, graphics, sprites, palettes, fonts, sounds, music, storyline and texts, even the font in the video trailer and drum sound samples for the soundtrack, completely myself from scratch, using completely and exclusive free as in freedom software, without accepting any contributions, with the goal of creating a completely original art which is not a derivative work of anyone else's existing work, so that I could assure that by waiving my intellectual property rights the work will become completely public domain with as little doubt as posible.

This work's goal is to never be encumbered by any exclusive intellectual property rights, it is intended to always stay completely and forever in the public domain, available for any use whatsoever.

I therefore release everything in this repository under CC0 1.0 (public domain, https://creativecommons.org/publicdomain/zero/1.0/) + a waiver of all other IP rights (including patents), which is as follows:

*Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.*

I would like to ask you, without it being any requirement at all, to please support free software and free culture by sharing at least some of your own work in a similar way I do with this project.